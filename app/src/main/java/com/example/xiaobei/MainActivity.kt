package com.example.xiaobei

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.InputType
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import java.io.File

class MainActivity : AppCompatActivity() {
    private var isGenerating = false
    private val handler = Handler(Looper.getMainLooper())
    private var primes = mutableListOf<Int>()
    private var start = 0
    private var end = 0
    private val history = mutableListOf<String>()
    private val fileName = "primes_history.txt"

    @SuppressLint("ResourceType")
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.xml.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_history -> {
                val intent = Intent(this, MainActivity2::class.java)
                intent.putStringArrayListExtra("history", ArrayList(history))
                intent.putExtra("start", start)
                intent.putExtra("end", end)
                intent.putIntegerArrayListExtra("primes", ArrayList(primes))
                startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main1)

        val editTextDate = findViewById<EditText>(R.id.editTextDate)
        val editTextDate2 = findViewById<EditText>(R.id.editTextDate2)
        val listView = findViewById<ListView>(R.id.listview)
        val buttonStart = findViewById<Button>(R.id.begin)
        val buttonStop = findViewById<Button>(R.id.stop)

        editTextDate.inputType = InputType.TYPE_CLASS_NUMBER
        editTextDate2.inputType = InputType.TYPE_CLASS_NUMBER

        // Load history from file
        loadHistory()
        updateListView(listView)

        buttonStart.setOnClickListener {
            if (!isGenerating) {
                isGenerating = true
                primes.clear()
                start = editTextDate.text.toString().toInt()
                end = editTextDate2.text.toString().toInt()
                Thread {
                    for (i in start..end) {
                        if (!isGenerating) break
                        if (isPrime(i)) {
                            primes.add(i)
                            handler.post {
                                updateListView(listView)
                            }
                            Thread.sleep(1000) // 每1秒生成一个素数
                        }
                    }
                    val record = "$start-$end: ${primes.joinToString(", ")}"
                    history.add(record)
                    saveHistory(record)
                }.start()
            }
        }

        buttonStop.setOnClickListener {
            isGenerating = false
        }
    }

    private fun isPrime(num: Int): Boolean {
        if (num < 2) return false
        for (i in 2..Math.sqrt(num.toDouble()).toInt()) {
            if (num % i == 0) return false
        }
        return true
    }

    private fun saveHistory(record: String) {
        val file = File(filesDir, fileName)
        file.appendText("$record\n")
    }

    private fun loadHistory() {
        val file = File(filesDir, fileName)
        if (file.exists()) {
            file.forEachLine { line ->
                history.add(line)
                val parts = line.split(": ")
                if (parts.size == 2) {
                    val range = parts[0].split("-")
                    if (range.size == 2) {
                        start = range[0].toInt()
                        end = range[1].toInt()
                        primes = parts[1].split(", ").map { it.toInt() }.toMutableList()
                    }
                }
            }
        }
    }

    private fun updateListView(listView: ListView) {
        val adapter = ArrayAdapter(this@MainActivity, android.R.layout.simple_list_item_1, primes)
        listView.adapter = adapter
    }

}