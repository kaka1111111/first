package com.example.xiaobei

import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)


        val textView = findViewById<TextView>(R.id.textView)
        val textView2 = findViewById<TextView>(R.id.textView3)
        val history = intent.getStringArrayListExtra("history") ?: arrayListOf()
        val previousPrime = intent.getIntExtra("start", 0)
        val nextPrime = intent.getIntExtra("end", 0)

        textView.text = previousPrime.toString()
        textView2.text = nextPrime.toString()


        val listView = findViewById<ListView>(R.id.listview)
        val primes = intent.getIntegerArrayListExtra("primes") ?: arrayListOf()

        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, primes)
        listView.adapter = adapter
    }

}